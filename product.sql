CREATE TABLE `products` (
  `product_id` int PRIMARY KEY AUTO_INCREMENT,
  `sku` varchar(255) UNIQUE,
  `name` varchar(255),
  `product_type` varchar(255),
  `price` float(15,2)
);

CREATE TABLE `product_attributes` (
  `attribute_id` int PRIMARY KEY AUTO_INCREMENT,
  `attribute_name` varchar(255)
);

CREATE TABLE `product_values` (
  `value_id` int PRIMARY KEY AUTO_INCREMENT,
  `product_sku` varchar(255),
  `attribute_id` int,
  `value` varchar (255)
);

ALTER TABLE `product_values` ADD CONSTRAINT attribute_value_id FOREIGN KEY (`attribute_id`) REFERENCES `product_attributes` (`attribute_id`) ON DELETE CASCADE;

ALTER TABLE `product_values` ADD CONSTRAINT value_prduct_sku FOREIGN KEY (`product_sku`) REFERENCES `products` (`sku`) ON DELETE CASCADE;

INSERT INTO product_attributes (attribute_name)
VALUES ('size'),('weight'),('height'),('width'),('length');
