# Scandiweb Junior developer test
A fully functional web page that reads, writes, displays and deletes data from database.
This project is a junior developer test that is fallowing certain criteria listed in this link:
<a href="https://www.notion.so/Junior-Developer-Test-Task-1b2184e40dea47df840b7c0cc638e61e">Junior Developer Test</a>

The project was built with OOP and SOLID principles in mind, and the code Follows <a href="https://www.php-fig.org/psr/psr-12/">PSR-12</a> standards.
The databases data modal is based on EAV.

## Technologies used
* PHP
* SASS
* JavaScript
* Docker
* MySQL
* Webpack

## How to set up the project locally 

1. Clone the project with Git clone.
````
git clone git@bitbucket.org:ArtisOzolins1/jakedu2021-2.git
````
2. Clone the project then switch to the newest branch.
````
git checkout [branch name]
````
3. Pull all the newest changes.
````
git pull origin [branch name]
````
4. Build the docker container.
````
docker-compose build 
````
5. Start the docker container.
````
docker-compose up -d 
````
6. Compile scss to css.
````
npm install
npm run build
````
If you are making changes to the scss file you can run the command below, so you dont have to run the privouse command over and over for each change.
````
npm run build:watch
````
7. To build the database enter a running docker container that's responsible for the database.
````
docker exec -it [container name] bash
````
8. After entering the docker container, enter the mysql and build the database from product.sql
````
mysql -uroot -proot
use jakedu;
[Copy and past contents of product.sql]
show tables;
````
After running show tables command you should see 3 tables in the database if so you have successfully made the database.

After making the database you should check the config.php file to make sure the database name, username and password is correct.

If you are making changes to the code make sure to turn on the developer mode in bootstrap.php so the errors and exceptions show.

Now if you have fallowed the set up guide going to <a href="http://localhost/">localhost</a> in your web browser should open the web application.

A common issue might be that the 80 port is taken you should free the port or change the docker-compose file to a free port.

## Known issues

* no known issues as of right now 

## Work in progress

* Change the way products in product list are fetched from database currently this is done through 3 different database mapper classes.
* Add even more validation rules.
* Add edit feature.