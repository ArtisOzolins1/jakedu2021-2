<?php

/**
 * Class Template
 */
class Template
{
    /**
     * @var string $templatePath
     */
    public string $templatePath = __DIR__ . '/template';

    /**
     * @var array $data
     */
    public array $data;

    /**
     * @var array $messages
     */
    public array $messages = [];

    /**
     * @param string $path
     * @param array $data
     */
    public function render(string $path, array $data): void
    {
        $this->data = $data;
        extract($data, EXTR_SKIP);
        ob_start();
        include($this->templatePath . $path);
        $this->clearFlashMessages('error');
        $this->clearFlashMessages('invalidData');
        echo ob_get_clean();
    }

    /**
     * @param string $type
     * @param string $messageName
     * @param string $message
     */
    public function addFlashMessage(string $type, string $messageName, string $message): void
    {
        $_SESSION[$type][$messageName][] = $message;
    }

    /**
     * @param string $type
     * @param string $dataName
     * @param string $data
     */
    public function addInvalidData(string $type, string $dataName, string $data): void
    {
        $_SESSION[$type][$dataName][] = $data;
    }

    /**
     * @param string $type
     * @param string $messageName
     * @return mixed
     */
    public function getFlashMessage(string $type, string $messageName)
    {
        return $_SESSION[$type][$messageName];
    }

    /**
     * @param string $type
     */
    public function clearFlashMessages(string $type): void
    {
        unset($_SESSION[$type]);
    }

    /**
     * @return mixed
     */
    public function renderPageNotFound()
    {
        return require_once($this->templatePath . '/page-not-found.php');
    }
}