<?php

namespace Router;

/**
 * Class Request
 * @package Router
 */
class Request
{
    /**
     * @var array
     */
    private array $params = [];

    /**
     * @param array $params
     * @return $this
     */
    public function setParams(array $params): Request
    {
        $this->params = $params;

        return $this;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @param string $param
     * @return string
     */
    public function getParam(string $param): string
    {
        return $this->params[$param];
    }
}