<?php

namespace Router;

use App;
use Template;

/**
 * Class Router
 * @package Router
 */
class Router
{
    /**
     * @var array $routes
     */
    private array $routes = [];

    /**
     * @var Request $request
     */
    private Request $request;

    /**
     * @var App $app
     */
    private App $app;

    /**
     * @var Response $response
     */
    private Response $response;

    private const METHOD_POST = 'POST';
    private const METHOD_GET = 'GET';
    private const VARIABLE_NAME_REGEX = '/\{(\w+)(:[^}]+)?}/';
    private const VARIABLE_NAME_AND_GROUPING_REGEX = '/\{(\w+)(:([^}]+))?}/';

    /**
     * Router constructor.
     * @param Request $request
     * @param App $app
     * @param Response $response
     */
    public function __construct(Request $request, App $app, Response $response)
    {
        $this->request = $request;
        $this->app = $app;
        $this->response = $response;
    }

    /**
     * @param string $path
     * @param $callback
     */
    public function get(string $path, $callback): void
    {
        $this->routes[self::METHOD_GET][$path] = $callback;
    }

    /**
     * @param string $path
     * @param $callback
     */
    public function post(string $path, $callback): void
    {
        $this->routes[self::METHOD_POST][$path] = $callback;
    }

    /**
     * @return false|mixed
     */
    public function getCallback()
    {
        $uri = parse_url($_SERVER['REQUEST_URI']);
        $path = $uri['path'];
        $method = $_SERVER['REQUEST_METHOD'];
        $path = trim($path, '/');
        $routes = $this->routes[$method] ?? [];
        $routeParams = false;

        foreach ($routes as $route => $callback) {
            $route = trim($route, '/');
            $routeNames = [];

            if (preg_match_all(self::VARIABLE_NAME_REGEX, $route, $matches)) {
                $routeNames = $matches[1];
            }

            $routeRegex = "@^" . preg_replace_callback(self::VARIABLE_NAME_AND_GROUPING_REGEX, fn($m) => isset($m[2]) ? "({$m[2]})" : '(\w+)', $route) . "$@";

            if (preg_match_all($routeRegex, $path, $valueMatches)) {
                $values = [];

                for ($i = 1; $i < count($valueMatches); $i++) {
                    $values[] = $valueMatches[$i][0];
                }
                $routeParams = array_combine($routeNames, $values);
                $this->request->setParams($routeParams);

                return $callback;
            }
        }

        return false;
    }

    /**
     * @return false|mixed|void
     */
    public function resolve()
    {
        $uri = parse_url($_SERVER['REQUEST_URI']);
        $path = $uri['path'];
        $method = $_SERVER['REQUEST_METHOD'];
        $callback = $this->routes[$method][$path];

        if (!$callback) {
            $callback = $this->getCallback();
        }

        if ($callback === false) {
            return $this->app->get(Template::class)->renderPageNotFound();
        }

        if (is_array($callback)) {
            $callback[0] = new $callback[0];
        }

        return (new $callback())->execute($this->request, $this->app, $this->response);
    }
}