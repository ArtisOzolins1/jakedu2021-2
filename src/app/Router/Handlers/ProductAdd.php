<?php

namespace Router\Handlers;

use App;
use Router\Request;
use Router\Response;
use Template;

/**
 * Class ProductAdd
 * @package Router\Handlers
 */
class ProductAdd extends AbstractController
{
    /**
     * @param Request $request
     * @param App $app
     * @param Response $response
     * @return false|string
     */
    public function execute(Request $request, App $app, Response $response)
    {
        return $app->get(Template::class)->render('/product-add.php', []);
    }
}