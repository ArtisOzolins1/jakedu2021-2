<?php

namespace Router\Handlers;

use App;
use Components\Factory\ProductTypeFactory;
use Exception;
use Router\Request;
use Router\Response;
use Service\BookService;
use Service\DvdService;
use Service\FurnitureService;

/**
 * Class ProductSave
 * @package Router\Handlers
 */
class ProductSave extends AbstractController
{
    /**
     * @param Request $request
     * @param App $app
     * @param Response $response
     * @throws Exception
     */
    public function execute(Request $request, App $app, Response $response)
    {
        try {
            $productTypeFactory = new ProductTypeFactory($app);
            $validatedData = $productTypeFactory->create($_POST["productType"], $_POST,);

            if (empty($validatedData)) {
                $response->redirect('/add-product');
            }
            if ($_POST["productType"] === "Book") {
                $app->get(BookService::class)->saveBook($validatedData);
            }
            if ($_POST["productType"] === "DVD") {
                $app->get(DvdService::class)->saveDvd($validatedData);
            }
            if ($_POST["productType"] === "Furniture") {
                $app->get(FurnitureService::class)->saveFurniture($validatedData);
            }
            $response->redirect('/');
        } catch (Exception $e) {
            throw new Exception('Could not insert product into product table', 0, $e);
        }
    }
}