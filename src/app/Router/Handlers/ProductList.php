<?php

namespace Router\Handlers;

use App;
use Exception;
use Router\Request;
use Router\Response;
use Service\BookService;
use Service\DvdService;
use Service\FurnitureService;
use Template;

/**
 * Class ProductList
 * @package Router\Handlers
 */
class ProductList extends AbstractController
{
    /**
     * @param Request $request
     * @param App $app
     * @param Response $response
     * @return mixed
     * @throws Exception
     */
    public function execute(Request $request, App $app, Response $response)
    {
        try {
            $dvdCollection = $app->get(DvdService::class)->fetchDvds();
            $bookCollection = $app->get(BookService::class)->fetchBooks();
            $furnitureCollection = $app->get(FurnitureService::class)->fetchFurniture();
            $allCollections = array_merge($dvdCollection, $bookCollection, $furnitureCollection);

            return $app->get(Template::class)->render('/product-list.php', $allCollections);
        } catch (Exception $e) {
            throw new Exception('Could not fetch data from product table', 0, $e);
        }
    }
}