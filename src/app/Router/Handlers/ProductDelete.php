<?php

namespace Router\Handlers;

use App;
use Exception;
use Router\Request;
use Router\Response;
use Service\ProductService;

/**
 * Class ProductDelete
 * @package Router\Handlers
 */
class ProductDelete extends AbstractController
{
    /**
     * @param Request $request
     * @param App $app
     * @param Response $response
     * @throws Exception
     */
    public function execute(Request $request, App $app, Response $response)
    {
        try {
            if (empty($_POST['checkbox'])) {
                $response->redirect("/");
            }
            $app->get(ProductService::class)->deleteProduct($_POST['checkbox']);
            $response->redirect("/");
        } catch (Exception $e) {
            throw new Exception('Could not delete product by id from product table', 0, $e);
        }
    }
}