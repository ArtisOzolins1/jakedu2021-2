<?php

namespace Router\Handlers;

use App;
use Router\Request;
use Router\Response;

/**
 * Class AbstractController
 * @package Router\Handlers
 */
abstract class AbstractController
{
    /**
     * @param Request $request
     * @param App $app
     * @param Response $response
     * @return mixed
     */
    abstract function execute(Request $request, App $app, Response $response);
}