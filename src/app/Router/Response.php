<?php

namespace Router;

/**
 * Class Response
 * @package Router
 */
class Response
{
    /**
     * @param string $path
     */
    public function redirect(string $path): void
    {
        header("Location:$path");
    }
}