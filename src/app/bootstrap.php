<?php

use Router\Request;
use Router\Response;
use Router\Router;

include 'config.php';

session_start();

spl_autoload_register(function ($className) {
    $docroot = dirname($_SERVER['DOCUMENT_ROOT']) . DIRECTORY_SEPARATOR . 'app';
    $instance = str_replace("\\", DIRECTORY_SEPARATOR, $className) . ".php";
    require_once $docroot . DIRECTORY_SEPARATOR . $instance;
});

if (isDeveloperMode) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

try {
    $request = new Request();
    $app = new App();
    $response = new Response();
    $router = new Router($request, $app, $response);
    $dependencies = include "dependencies.php";
    foreach ($dependencies as $dependence => $object) {
        $app->register($dependence, $object);
    }
    $uri = $_SERVER['REQUEST_URI'];
    require_once "routes.php";
    $router->resolve();
} catch (Exception $e) {
    if (isDeveloperMode) {
        echo "<pre>Exception: ", $e->getMessage(), " in ", $e->getFile(), " on line ", $e->getLine(),
        "\nStack trace:\n", $e->getTraceAsString(), "</pre><pre>", $e->getPrevious(), "</pre>\n";
    } else {
        echo "Whoops, looks like something went wrong.";
    }
    file_put_contents('../logs/error-log.log', $e->getTraceAsString(), FILE_APPEND);
}