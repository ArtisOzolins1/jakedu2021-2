<?php

namespace Validator;

/**
 * Interface ValidationRules
 * @package Validator
 */
interface ValidationRules
{
    /**
     * @return mixed
     */
    public function validateProduct();
}