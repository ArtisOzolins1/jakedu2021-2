<?php

namespace Validator;

use Template;

/**
 * Class Validation
 * @package Validator
 */
class Validation
{
    /**
     * @var array $validatorStatus
     */
    public array $validatorStatus = [];

    /**
     * @var bool $productValidationStatus
     */
    public bool $productValidationStatus = true;

    /**
     * @var Template $template
     */
    private Template $template;

    /**
     * Validation constructor.
     * @param Template $template
     */
    public function __construct(Template $template)
    {
        $this->template = $template;
    }

    /**
     * @param $constraint
     */
    public function handle($constraint): void
    {
        $this->addValidationStatus($constraint->isValid($this->template));
    }

    /**
     * @param $validationStatus
     */
    public function addValidationStatus($validationStatus): void
    {
        $this->validatorStatus[] = $validationStatus;
    }

    /**
     * @return bool
     */
    public function validate(): bool
    {
        foreach ($this->validatorStatus as $value) {
            if ($value['errorStatus'] !== true) {
                return $this->productValidationStatus = $value['errorStatus'];
            }
        }

        return $this->productValidationStatus;
    }

    /**
     * @return bool
     */
    public function isProductValid(): bool
    {
        return $this->productValidationStatus;
    }
}