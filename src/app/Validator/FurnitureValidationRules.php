<?php

namespace Validator;

use Service\ProductService;
use Validator\Rules\IsAlphanumerical;
use Validator\Rules\IsNumeric;
use Validator\Rules\IsRequired;
use Validator\Rules\IsUnique;

/**
 * Class FurnitureValidationRules
 * @package Validator
 */
class FurnitureValidationRules implements ValidationRules
{
    /**
     * @var array $data
     */
    private array $data;

    /**
     * @var Validation $validation
     */
    private Validation $validation;

    /**
     * @var ProductService
     */
    private ProductService $productService;

    /**
     * FurnitureValidationRules constructor.
     * @param array $data
     * @param Validation $validation
     * @param ProductService $productService
     */
    public function __construct(array $data, Validation $validation, ProductService $productService)
    {
        $this->data = $data;
        $this->validation = $validation;
        $this->productService = $productService;
    }

    /**
     * @return array
     */
    public function validateProduct(): array
    {
        $this->validation->handle(new IsUnique($this->data['sku'], $this->productService->fetchSkuBySku($this->data['sku']), 'sku', 'This field needs to be unique'));
        $this->validation->handle(new IsRequired($this->data['sku'], 'sku', 'This field is required'));
        $this->validation->handle(new IsAlphanumerical($this->data['sku'], 'sku', 'This field needs to be alphanumeric'));
        $this->validation->handle(new IsRequired($this->data['name'], 'name', 'This field is required'));
        $this->validation->handle(new IsAlphanumerical($this->data['name'], 'name', 'This field needs to be alphanumeric'));
        $this->validation->handle(new IsRequired($this->data['price'], 'price', 'This field is required'));
        $this->validation->handle(new IsNumeric($this->data['price'], 'price', 'This field needs to be numeric'));
        $this->validation->handle(new IsRequired($this->data['height'], 'height', 'This field is required'));
        $this->validation->handle(new IsNumeric($this->data['height'], 'height', 'This field needs to be numeric'));
        $this->validation->handle(new IsRequired($this->data['width'], 'width', 'This field is required'));
        $this->validation->handle(new IsNumeric($this->data['width'], 'width', 'This field needs to be numeric'));
        $this->validation->handle(new IsRequired($this->data['length'], 'length', 'This field is required'));
        $this->validation->handle(new IsNumeric($this->data['length'], 'length', 'This field needs to be numeric'));
        $this->validation->validate();

        if ($this->validation->isProductValid()) {
            return $this->data;
        }

        return [];
    }
}