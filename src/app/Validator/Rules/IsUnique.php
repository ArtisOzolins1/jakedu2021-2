<?php

namespace Validator\Rules;

use Template;

/**
 * Class IsUnique
 * @package Validator\Rules
 */
class IsUnique implements Constraint
{
    /**
     * @var bool $isDataValid
     */
    private bool $isDataValid = true;

    /**
     * @var string $data
     */
    private string $data;

    /**
     * @var string $compareData ;
     */
    private string $compareData;

    /**
     * @var string $errorName
     */
    private string $errorName;

    /**
     * @var string $errorMsg
     */
    private string $errorMsg;

    /**
     * isUnique constructor.
     * @param string $data
     * @param string $compareData
     * @param string $errorName
     * @param string $errorMsg
     */
    public function __construct(string $data, string $compareData, string $errorName, string $errorMsg)
    {
        $this->data = $data;
        $this->compareData = $compareData;
        $this->errorName = $errorName;
        $this->errorMsg = $errorMsg;
    }

    /**
     * @param Template $template
     * @return array
     */
    public function isValid(Template $template): array
    {
        $template->addInvalidData('invalidData', $this->errorName, $this->data);
        if ($this->data === $this->compareData) {
            $template->addFlashMessage('error', $this->errorName, $this->errorMsg);
            $this->isDataValid = false;
        }

        return [
            'errorName' => $this->errorName,
            'errorStatus' => $this->isDataValid,
        ];
    }
}