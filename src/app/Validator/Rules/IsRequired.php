<?php

namespace Validator\Rules;

use Template;

/**
 * Class IsRequired
 * @package Validator\Rules
 */
class IsRequired implements Constraint
{
    /**
     * @var bool $isDataValid
     */
    private bool $isDataValid = true;

    /**
     * @var $data
     */
    private $data;

    /**
     * @var string $errorName
     */
    private string $errorName;

    /**
     * @var string $errorMsg
     */
    private string $errorMsg;

    /**
     * isRequired constructor.
     * @param $data
     * @param string $errorName
     * @param string $errorMsg
     */
    public function __construct($data, string $errorName, string $errorMsg)
    {
        $this->data = $data;
        $this->errorName = $errorName;
        $this->errorMsg = $errorMsg;
    }

    /**
     * @param Template $template
     * @return array
     */
    public function isValid(Template $template): array
    {
        $template->addInvalidData('invalidData', $this->errorName, $this->data);
        if (empty($this->data)) {
            $template->addFlashMessage('error', $this->errorName, $this->errorMsg);
            $this->isDataValid = false;
        }

        return [
            'errorName' => $this->errorName,
            'errorStatus' => $this->isDataValid,
        ];
    }
}
