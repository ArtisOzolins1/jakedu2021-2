<?php

namespace Validator\Rules;

use Template;

/**
 * Interface Constraint
 * @package Validator\Rules
 */
interface Constraint
{
    /**
     * @param Template $template
     * @return mixed
     */
    public function isValid(Template $template);
}