<?php

namespace Validator\Rules;

use Template;

/**
 * Class IsAlphanumerical
 * @package Validator\Rules
 */
class IsAlphanumerical implements Constraint
{
    /**
     * @var bool $isDataValid
     */
    private bool $isDataValid = true;

    /**
     * @var string $AlphanumericRegex
     */
    private string $AlphanumericRegex = "/[^a-z_\-0-9 ]/i";

    /**
     * @var string $data
     */
    private string $data;

    /**
     * @var string $errorName
     */
    private string $errorName;

    /**
     * @var string $errorMsg
     */
    private string $errorMsg;

    /**
     * isPositive constructor.
     * @param string $data
     * @param string $errorName
     * @param string $errorMsg
     */
    public function __construct(string $data, string $errorName, string $errorMsg)
    {
        $this->data = $data;
        $this->errorName = $errorName;
        $this->errorMsg = $errorMsg;
    }

    /**
     * @param Template $template
     * @return array
     */
    public function isValid(Template $template): array
    {
        $template->addInvalidData('invalidData', $this->errorName, $this->data);
        if (preg_match($this->AlphanumericRegex, $this->data) === 1) {
            $template->addFlashMessage('error', $this->errorName, $this->errorMsg);
            $this->isDataValid = false;
        }

        return [
            'errorName' => $this->errorName,
            'errorStatus' => $this->isDataValid,
        ];
    }
}