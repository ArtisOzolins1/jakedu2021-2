<?php

namespace Validator\Rules;

use Template;

/**
 * Class IsRequiredLength
 * @package Validator\Rules
 */
class IsRequiredLength implements Constraint
{
    /**
     * @var bool $isDataValid
     */
    private bool $isDataValid = true;

    /**
     * @var string $data
     */
    private string $data;

    /**
     * @var int $stringLength
     */
    private int $stringLength;

    /**
     * @var string $errorName
     */
    private string $errorName;

    /**
     * @var string $errorMsg
     */
    private string $errorMsg;

    /**
     * isRequiredLength constructor.
     * @param $data
     * @param $stringLength
     * @param $errorName
     * @param $errorMsg
     */
    public function __construct($data, $stringLength, $errorName, $errorMsg)
    {
        $this->data = $data;
        $this->stringLength = $stringLength;
        $this->errorName = $errorName;
        $this->errorMsg = $errorMsg;
    }

    /**
     * @param Template $template
     * @return array
     */
    public function isValid(Template $template): array
    {
        $template->addInvalidData('invalidData', $this->errorName, $this->data);
        if (strlen($this->data) < $this->stringLength) {
            $template->addFlashMessage('error', $this->errorName, $this->errorMsg);
            $this->isDataValid = false;
        }

        return [
            'errorName' => $this->errorName,
            'errorStatus' => $this->isDataValid,
        ];
    }
}
