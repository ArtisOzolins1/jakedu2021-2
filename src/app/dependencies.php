<?php

use Service\BookService;
use Service\DvdService;
use Service\FurnitureService;
use Service\ProductService;
use Validator\Validation;

return [
    Template::class => function () {
        if (!isset($template)) {
            $template = new Template();
        }

        return $template;
    },
    Validation::class => function (App $app) {
        if (!isset($validation)) {
            $validation = new Validation($app->get(Template::class));
        }

        return $validation;
    },
    DvdService::class => function (App $app) {
        if (!isset($dvdService)) {
            $dvdService = new DvdService($app->get(PDO::class));
        }

        return $dvdService;
    },
    BookService::class => function (App $app) {
        if (!isset($bookService)) {
            $bookService = new BookService($app->get(PDO::class));
        }

        return $bookService;
    },
    FurnitureService::class => function (App $app) {
        if (!isset($furnitureService)) {
            $furnitureService = new FurnitureService($app->get(PDO::class));
        }

        return $furnitureService;
    },
    ProductService::class => function (App $app) {
        if (!isset($furnitureService)) {
            $furnitureService = new ProductService($app->get(PDO::class));
        }

        return $furnitureService;
    },
    PDO::class => function () {
        if (!isset($pdo)) {
            $options = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
            $pdo = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD, $options);
        }

        return $pdo;
    }
];