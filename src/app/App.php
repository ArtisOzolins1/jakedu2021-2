<?php

/**
 * Class App
 */
final class App
{
    /**
     * @var array $dependencies
     */
    private array $dependencies = [];

    /**
     * @param string $dependence
     * @param object $callback
     */
    public function register(string $dependence, object $callback): void
    {
        $this->dependencies[$dependence] = $callback;
    }

    /**
     * @param string $dependence
     * @return false|mixed
     */
    public function get(string $dependence)
    {
        $callback = $this->dependencies[$dependence];

        return call_user_func($callback, $this);
    }
}