<?php

namespace Service;

use Exception;
use Model\Domain\Product;
use Model\Persistence\ProductInDatabase;
use PDO;

/**
 * Class ProductService
 * @package Service
 */
class ProductService
{
    /**
     * @var PDO $pdo
     */
    private PDO $pdo;

    /**
     * ProductService constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param array $checkbox
     * @throws Exception
     */
    public function deleteProduct(array $checkbox): void
    {
        $data = new Product();
        $mapper = new ProductInDatabase($this->pdo);
        foreach ($checkbox as $productId) {
            $data->setId($productId);
            $mapper->deleteProduct($data);
        }
    }

    /**
     * @return array
     */
    public function fetchProducts(): array
    {
        $mapper = new ProductInDatabase($this->pdo);

        return $mapper->fetchAllProducts();
    }

    /**
     * @param string $sku
     * @return string
     */
    public function fetchSkuBySku(string $sku): string
    {
        $mapper = new ProductInDatabase($this->pdo);

        return $mapper->fetchSkuBySku($sku);
    }
}