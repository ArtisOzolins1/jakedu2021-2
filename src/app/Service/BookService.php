<?php

namespace Service;

use Components\EavService;
use Exception;
use Model\Domain\Book;
use Model\Persistence\BookDTO;
use Model\Persistence\BookInDatabase;
use PDO;

/**
 * Class BookService
 * @package Service
 */
class BookService
{
    /**
     * @var PDO $pdo
     */
    private PDO $pdo;

    /**
     * BookService constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param array{
     * sku: string,
     * name: string,
     * price: float,
     * productType: string,
     * weight: float
     * } $data
     * @throws Exception
     */
    public function saveBook(array $data): void
    {
        $book = Book::saveBook($data['sku'], $data['name'], $data['price'], $data['productType'], $data['weight']);
        $mapper = new BookInDatabase($this->pdo, new EavService($this->pdo));
        $mapper->insert($book);
    }

    /**
     * @return array|BookDTO[]
     */
    public function fetchBooks(): array
    {
        $mapper = new BookInDatabase($this->pdo, new EavService($this->pdo));

        return $mapper->fetchAll();
    }
}