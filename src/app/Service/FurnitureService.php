<?php

namespace Service;

use Components\EavService;
use Exception;
use Model\Domain\Furniture;
use Model\Persistence\FurnitureDTO;
use Model\Persistence\FurnitureInDatabase;
use PDO;

/**
 * Class FurnitureService
 * @package Service
 */
class FurnitureService
{
    /**
     * @var PDO $pdo
     */
    private PDO $pdo;

    /**
     * FurnitureService constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param array{
     * sku: string,
     * name: string,
     * price: float,
     * productType: string,
     * height: float,
     * width: float,
     * length: float
     * } $data
     * @throws Exception
     */
    public function saveFurniture(array $data): void
    {
        $furniture = Furniture::saveFurniture($data['sku'], $data['name'], $data['price'], $data['productType'], $data['height'], $data['width'], $data['length']);
        $mapper = new FurnitureInDatabase($this->pdo, new EavService($this->pdo));
        $mapper->insert($furniture);
    }

    /**
     * @return array|FurnitureDTO[]
     */
    public function fetchFurniture(): array
    {
        $mapper = new FurnitureInDatabase($this->pdo, new EavService($this->pdo));

        return $mapper->fetchAll();
    }
}