<?php

namespace Service;

use Components\EavService;
use Exception;
use Model\Domain\Dvd;
use Model\Persistence\DvdDTO;
use Model\Persistence\DvdInDatabase;
use PDO;

/**
 * Class DvdService
 * @package Service
 */
class DvdService
{
    /**
     * @var PDO $pdo
     */
    private PDO $pdo;

    /**
     * DvdService constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param array{
     * sku: string,
     * name: string,
     * price: float,
     * productType: string,
     * size: float
     * } $data
     * @throws Exception
     */
    public function saveDvd(array $data): void
    {
        $dvd = Dvd::saveDvd($data['sku'], $data['name'], $data['price'], $data['productType'], $data['size']);
        $mapper = new DvdInDatabase($this->pdo, new EavService($this->pdo));
        $mapper->insert($dvd);
    }

    /**
     * @return array|DvdDTO[]
     */
    public function fetchDvds(): array
    {
        $mapper = new DvdInDatabase($this->pdo, new EavService($this->pdo));

        return $mapper->fetchAll();
    }
}