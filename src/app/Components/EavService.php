<?php

namespace Components;

use PDO;

/**
 * Class EavService
 * @package Components
 */
class EavService
{
    /**
     * @var PDO $pdo
     */
    private PDO $pdo;

    /**
     * EavService constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param string $sku
     * @param string $name
     * @param string $productType
     * @param float $price
     */
    public function saveMainAttributes(string $sku, string $name, string $productType, float $price): void
    {
        $query = $this->pdo->prepare("
            INSERT INTO products (sku, name, product_type, price) 
            VALUES (:sku, :name, :product_type, :price);
        ");
        $data = [
            'sku' => $sku,
            'name' => $name,
            'product_type' => $productType,
            'price' => $price
        ];
        $query->execute($data);
    }

    /**
     * @param string $sku
     * @param int $attributeId
     * @param string $attributeValue
     */
    public function saveAttributes(string $sku, int $attributeId, string $attributeValue): void
    {
        $query = $this->pdo->prepare("
            INSERT INTO product_values (product_sku, attribute_id, value)
            VALUES(:product_sku, :attribute_id, :value);
        ");

        $data = [
            'product_sku' => $sku,
            'attribute_id' => $attributeId,
            'value' => $attributeValue,
        ];
        $query->execute($data);
    }
}