<?php

namespace Components\Factory;

use App;
use Service\BookService;
use Service\DvdService;
use Service\FurnitureService;
use Service\ProductService;
use Router\Response;
use Validator\DvdValidationRules;
use Validator\BookValidationRules;
use Validator\FurnitureValidationRules;
use Validator\Validation;

/**
 * Class ProductTypeFactory
 * @package Components\Factory
 */
class ProductTypeFactory
{
    /**
     * @var App
     */
    private App $app;

    /**
     * ProductTypeFactory constructor.
     * @param App $app
     */
    public function __construct(App $app)
    {
        $this->app = $app;
    }

    /**
     * @param string $productType
     * @param array $data
     * @return array
     */
    public function create(string $productType, array $data): array
    {
        switch ($productType) {
            case "Book":
                $bookValidation = new BookValidationRules($data, $this->app->get(Validation::class), $this->app->get(ProductService::class));
                return $validatedData = $bookValidation->validateProduct();
            case "DVD":
                $dvdValidation = new DvdValidationRules($data, $this->app->get(Validation::class), $this->app->get(ProductService::class));
                return $validatedData = $dvdValidation->validateProduct();
            case "Furniture":
                $furnitureValidation = new FurnitureValidationRules($data, $this->app->get(Validation::class), $this->app->get(ProductService::class));
                return $validatedData = $furnitureValidation->validateProduct();
        }
    }
}