<?php

namespace Components;

use PDO;

/**
 * Class ProductService
 * @package Components
 */
class ProductService
{
    /**
     * @var PDO $pdo
     */
    private PDO $pdo;

    /**
     * ProductService constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param string $productType
     * @return array
     */
    public function fetchProductByProductType(string $productType): array
    {
        $query = $this->pdo->prepare("
            SELECT pr.product_id, pr.name, pr.sku, pr.product_type, pr.price, pv.value, pa.attribute_name
            FROM products AS pr inner JOIN product_values AS pv
            ON pr.sku = pv.product_sku
            INNER JOIN product_attributes AS pa
            USING (attribute_id)
            WHERE pr.product_type = :product_type");
        $query->bindParam(":product_type", $productType);
        $query->execute();

        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
}