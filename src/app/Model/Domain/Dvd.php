<?php

namespace Model\Domain;

use Exception;

/**
 * Class Dvd
 * @package Model\Domain
 * @property-read float $size
 */
class Dvd extends Product
{
    const Product_Type = 'Dvd';

    /**
     * @var int $size
     */
    protected int $size;

    /**
     * @throws Exception
     */
    public function setSize(int $size): void
    {
        if ($size < 0) {
            throw new Exception('$size cant be less then 0');
        }
        $this->size = $size;
    }

    public function __get($value): string
    {
        return $this->$value;
    }


    /**
     * @param string $sku
     * @param string $name
     * @param float $price
     * @param string $productType
     * @param float $size
     * @return Dvd
     * @throws Exception
     */
    public static function saveDvd(string $sku, string $name, float $price, string $productType, float $size): Dvd
    {
        $dvd = new self;
        $dvd->setSku($sku);
        $dvd->setName($name);
        $dvd->setPrice($price);
        $dvd->setProductType($productType);
        $dvd->setSize($size);

        return $dvd;
    }
}