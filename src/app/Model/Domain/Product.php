<?php

namespace Model\Domain;

use Exception;

/**
 * @property-read int $id
 * @property-read string $sku
 * @property-read string $name
 * @property-read float $price
 * @property-read string $productType
 */
class Product
{
    /**
     * @var int $id
     */
    protected int $id;

    /**
     * @var string $sku
     */
    protected string $sku;

    /**
     * @var string $name
     */
    protected string $name;

    /**
     * @var float $price
     */
    protected float $price;

    /**
     * @var string $productType
     */
    protected string $productType;

    /**
     * @throws Exception
     */
    public function setId(int $id): void
    {
        if (!isset($id)) {
            throw new Exception ('$id is empty');
        }
        $this->id = $id;
    }

    /**
     * @throws Exception
     */
    public function setSku(string $sku): void
    {
        if (!isset($sku)) {
            throw new Exception ('$sku is empty');
        }
        if (strlen($sku) > 40) {
            throw new Exception ('$sku should not be bigger then 40 characters long');
        }
        $this->sku = $sku;
    }

    /**
     * @throws Exception
     */
    public function setName(string $name): void
    {
        if (!isset($name)) {
            throw new Exception ('$name is empty');
        }
        if (strlen($name) > 60) {
            throw new Exception ('$name should not be bigger then 60 characters long');
        }
        $this->name = $name;
    }

    /**
     * @throws Exception
     */
    public function setPrice(float $price): void
    {
        if (!isset($price)) {
            throw new Exception ('$price is empty');
        }
        $this->price = $price;
    }

    /**
     * @throws Exception
     */
    public function setProductType(string $productType): void
    {
        if (!isset($productType)) {
            throw new Exception ('$productType is empty');
        }
        $this->productType = $productType;
    }

    /**
     * @return mixed
     */
    public function __get($value): string
    {
        return $this->$value;
    }
}