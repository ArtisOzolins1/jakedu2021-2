<?php

namespace Model\Domain;

use Exception;

/**
 * Class Book
 * @package Model\Domain
 * @property-read float $weight
 */
class Book extends Product
{
    const Product_Type = 'Book';

    /**
     * @var float $weight
     */
    protected float $weight;

    /**
     * @throws Exception
     */
    public function setWeight(float $weight): void
    {
        if (!isset($weight)) {
            throw new Exception ('$weight is empty');
        }
        $this->weight = $weight;
    }

    public function __get($value): string
    {
        return $this->$value;
    }

    /**
     * @param string $sku
     * @param string $name
     * @param float $price
     * @param string $productType
     * @param float $weight
     * @return Book
     * @throws Exception
     */
    public static function saveBook(string $sku, string $name, float $price, string $productType, float $weight): Book
    {
        $book = new self;
        $book->setSku($sku);
        $book->setName($name);
        $book->setPrice($price);
        $book->setProductType($productType);
        $book->setWeight($weight);

        return $book;
    }
}