<?php

namespace Model\Domain;

use Exception;

/**
 * @property-read float $height
 * @property-read float $width
 * @property-read float $length
 */
class Furniture extends Product
{
    const Product_Type = 'Furniture';

    /**
     * @var float $height
     */
    protected float $height;

    /**
     * @var float $width
     */
    protected float $width;

    /**
     * @var float $length
     */
    protected float $length;

    /**
     * @throws Exception
     */
    public function setHeight(float $height): void
    {
        if ($height < 0) {
            throw new Exception('$height cant be less then 0');
        }
        $this->height = $height;
    }

    /**
     * @throws Exception
     */
    public function setWidth(float $width): void
    {
        if ($width < 0) {
            throw new Exception('$width cant be less then 0');
        }
        $this->width = $width;
    }

    /**
     * @throws Exception
     */
    public function setLength(float $length): void
    {
        if ($length < 0) {
            throw new Exception('$size cant be less then 0');
        }
        $this->length = $length;
    }

    public function __get($value): string
    {
        return $this->$value;
    }

    /**
     * @param string $sku
     * @param string $name
     * @param float $price
     * @param string $productType
     * @param float $height
     * @param float $width
     * @param float $length
     * @return Furniture
     * @throws Exception
     */
    public static function saveFurniture(string $sku, string $name, float $price, string $productType, float $height, float $width, float $length): Furniture
    {
        $furniture = new self;
        $furniture->setSku($sku);
        $furniture->setName($name);
        $furniture->setPrice($price);
        $furniture->setProductType($productType);
        $furniture->setHeight($height);
        $furniture->setWidth($width);
        $furniture->setLength($length);

        return $furniture;
    }
}