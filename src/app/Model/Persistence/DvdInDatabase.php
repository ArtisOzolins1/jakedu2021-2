<?php

namespace Model\Persistence;

use Components\EavService;
use Components\ProductService;
use Model\Domain\Dvd;
use Model\Domain\Product;
use PDO;

/**
 * Class DvdInDatabase
 * @package Model\Persistence
 */
class DvdInDatabase implements DatabaseInterface
{
    const SIZE_ID = 1;

    /**
     * @var PDO $pdo
     */
    private PDO $pdo;

    /**
     * @var EavService $eav
     */
    private EavService $eav;

    /**
     * DvdInDatabase constructor.
     * @param PDO $pdo
     * @param EavService $eav
     */
    public function __construct(PDO $pdo, EavService $eav)
    {
        $this->pdo = $pdo;
        $this->eav = $eav;
    }

    /**
     * @param Dvd $product
     */
    public function insert(Product $product): void
    {
        $this->eav->saveMainAttributes($product->sku, $product->name, $product->productType, $product->price);
        $this->eav->saveAttributes($product->sku, self::SIZE_ID, $product->size);
    }

    /**
     * @return array|DvdDTO[]
     */
    public function fetchAll(): array
    {
        $productService = new ProductService($this->pdo);
        $rows = $productService->fetchProductByProductType('DVD');

        $products = [];
        foreach ($rows as $row) {
            $product = new DvdDTO();
            $product->id = $row['product_id'];
            $product->sku = $row['sku'];
            $product->name = $row['name'];
            $product->price = $row['price'];
            $product->size = $row['value'];
            $product->productType = $row['product_type'];
            $products[] = $product;
        }

        return $products;
    }
}



