<?php

namespace Model\Persistence;

/**
 * Class FurnitureDTO
 * @package Model\Persistence
 */
class FurnitureDTO
{
    /**
     * @var int $id
     */
    public int $id;

    /**
     * @var string $sku
     */
    public string $sku;

    /**
     * @var string $name
     */
    public string $name;

    /**
     * @var float $price
     */
    public float $price;

    /**
     * @var string $productType
     */
    public string $productType;

    /**
     * @var float $height
     */
    public float $height;

    /**
     * @var float $width
     */
    public float $width;

    /**
     * @var float $length
     */
    public float $length;
}