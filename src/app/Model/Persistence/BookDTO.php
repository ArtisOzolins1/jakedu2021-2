<?php

namespace Model\Persistence;

/**
 * Class BookDTO
 * @package Model\Persistence
 */
class BookDTO
{
    /**
     * @var int $id
     */
    public int $id;

    /**
     * @var string $sku
     */
    public string $sku;

    /**
     * @var string $name
     */
    public string $name;

    /**
     * @var float $price
     */
    public float $price;

    /**
     * @var string $productType
     */
    public string $productType;

    /**
     * @var float $weight
     */
    public float $weight;
}