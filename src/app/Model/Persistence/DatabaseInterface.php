<?php

namespace Model\Persistence;

use Model\Domain\Book;
use Model\Domain\Dvd;
use Model\Domain\Furniture;
use Model\Domain\Product;

/**
 * Interface DatabaseInterface
 * @package Model\Persistence
 */
interface DatabaseInterface
{
    /**
     * @return mixed
     * @param Dvd|Book|Furniture $product
     */
    public function insert(Product $product);

    /**
     * @return mixed
     */
    public function fetchAll();
}
