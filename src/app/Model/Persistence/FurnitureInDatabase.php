<?php

namespace Model\Persistence;

use Components\EavService;
use Components\ProductService;
use Model\Domain\Furniture;
use Model\Domain\Product;
use PDO;

/**
 * Class FurnitureInDatabase
 * @package Model\Persistence
 */
class FurnitureInDatabase implements DatabaseInterface
{
    const HEIGHT_ID = 3;
    const WIDTH_ID = 4;
    const LENGTH_ID = 5;
    /**
     * @var PDO $pdo
     */
    private PDO $pdo;

    /**
     * @var EavService $eav
     */
    private EavService $eav;

    /**
     * @var float $height
     */
    private float $height;

    /**
     * @var float $width
     */
    private float $width;

    /**
     * @var float $length
     */
    private float $length;

    /**
     * FurnitureInDatabase constructor.
     * @param PDO $pdo
     * @param EavService $eav
     */
    public function __construct(PDO $pdo, EavService $eav)
    {
        $this->pdo = $pdo;
        $this->eav = $eav;
    }

    /**
     * @param Furniture $product
     */
    public function insert(Product $product): void
    {
        $this->eav->saveMainAttributes($product->sku, $product->name, $product->productType, $product->price);
        $this->eav->saveAttributes($product->sku, self::HEIGHT_ID, $product->height);
        $this->eav->saveAttributes($product->sku, self::WIDTH_ID, $product->width);
        $this->eav->saveAttributes($product->sku, self::LENGTH_ID, $product->length);
    }

    /**
     * @return array|FurnitureDTO[]
     */
    public function fetchAll(): array
    {
        $productService = new ProductService($this->pdo);
        $rows = $productService->fetchProductByProductType('furniture');

        $products = [];
        $attributeCounter = 0;
        $maxAttributeCount = 3;
        foreach ($rows as $row) {
            $attributeCounter++;
            if ($row['attribute_name'] === 'height') {
                $this->height = $row['value'];
            }
            if ($row['attribute_name'] === 'width') {
                $this->width = $row['value'];
            }
            if ($row['attribute_name'] === 'length') {
                $this->length = $row['value'];
            }
            if ($attributeCounter === $maxAttributeCount) {
                $product = new FurnitureDTO();
                $product->id = $row['product_id'];
                $product->sku = $row['sku'];
                $product->name = $row['name'];
                $product->productType = $row['product_type'];
                $product->price = $row['price'];
                $product->height = $this->height;
                $product->width = $this->width;
                $product->length = $this->length;
                $products[] = $product;
                $attributeCounter = 0;
            }
        }

        return $products;
    }
}