<?php

namespace Model\Persistence;

use Components\ProductService;
use Components\EavService;
use Model\Domain\Book;
use Model\Domain\Product;
use PDO;

/**
 * Class BookInDatabase
 * @package Model\Persistence
 */
class BookInDatabase implements DatabaseInterface
{
    const WEIGHT_ID = 2;

    /**
     * @var PDO $pdo
     */
    private PDO $pdo;

    /**
     * @var EavService $eav
     */
    private EavService $eav;

    /**
     * BookInDatabase constructor.
     * @param PDO $pdo
     * @param EavService $eav
     */
    public function __construct(PDO $pdo, EavService $eav)
    {
        $this->pdo = $pdo;
        $this->eav = $eav;
    }

    /**
     * @param Book $product
     */
    public function insert(Product $product): void
    {
        $this->eav->saveMainAttributes($product->sku, $product->name, $product->productType, $product->price);
        $this->eav->saveAttributes($product->sku, self::WEIGHT_ID, $product->weight);
    }

    /**
     * @return array|BookDTO[]
     */
    public function fetchAll(): array
    {
        $productService = new ProductService($this->pdo);

        $rows = $productService->fetchProductByProductType('book');

        $products = [];
        foreach ($rows as $row) {
            $product = new BookDTO();
            $product->id = $row['product_id'];
            $product->sku = $row['sku'];
            $product->name = $row['name'];
            $product->productType = $row['product_type'];
            $product->price = $row['price'];
            $product->weight = $row['value'];
            $products[] = $product;
        }

        return $products;
    }
}