<?php

namespace Model\Persistence;

use Model\Domain\Product;
use PDO;

/**
 * Class ProductInDatabase
 * @package Model\Persistence
 */
class ProductInDatabase
{
    /**
     * @var PDO $pdo
     */
    private PDO $pdo;

    /**
     * ProductInDatabase constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param Product $product
     */
    public function deleteProduct(Product $product): void
    {
        $id = $product->id;
        $query = $this->pdo->prepare("DELETE FROM products WHERE product_id=:product_id");
        $query->bindParam(":product_id", $id);
        $query->execute();
    }

    /**
     * @return array
     */
    public function fetchAllProducts(): array
    {
        $query = $this->pdo->prepare("
            SELECT pr.product_id, pr.name, pr.sku, pr.product_type, pr.price, pv.value, pa.attribute_name
            FROM products AS pr inner JOIN product_values AS pv
            ON pr.sku = pv.product_sku
            INNER JOIN product_attributes AS pa
            USING (attribute_id)
        ");
        $query->execute();

        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param string $sku
     * @return mixed
     */
    public function fetchSkuBySku(string $sku): string
    {
        $query = $this->pdo->prepare("SELECT sku FROM products WHERE sku=:sku");
        $query->bindParam(":sku", $sku);
        $query->execute();
        $row = $query->fetch(PDO::FETCH_ASSOC);
        if (empty($row)) {
            return "Sku not found";
        }

        return $row['sku'];
    }
}