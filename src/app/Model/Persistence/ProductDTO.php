<?php

namespace Model\Persistence;

/**
 * Class ProductDTO
 * @package Model\Persistence
 */
class ProductDTO
{
    /**
     * @var int $id
     */
    public int $id;

    /**
     * @var string $sku
     */
    public string $sku;

    /**
     * @var string $name
     */
    public string $name;

    /**
     * @var float $price
     */
    public float $price;

    /**
     * @var string $productType
     */
    public string $productType;
}