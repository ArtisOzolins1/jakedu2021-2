<?php

namespace Model\Persistence;

/**
 * Class DvdDTO
 * @package Model\Persistence
 */
class DvdDTO
{
    /**
     * @var int $id
     */
    public int $id;

    /**
     * @var string $sku
     */
    public string $sku;

    /**
     * @var string $name
     */
    public string $name;

    /**
     * @var float $price
     */
    public float $price;

    /**
     * @var string $productType
     */
    public string $productType;

    /**
     * @var float $size
     */
    public float $size;
}