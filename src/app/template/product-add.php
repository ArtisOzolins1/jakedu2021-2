<?php require_once "layout/head.php" ?>

    <form id="product_form" action="/add-product/save" method="post">
        <header>
            <div class="header-container">
                <div class="title">Product Add</div>
                <div class="button-prodAdd">
                    <input type="submit" value="Save">
                </div>
                <a href="/" class="button-prodAdd">
                    <input type="button" value="Cancel">
                </a>
            </div>
        </header>
        <main>
            <div class="baseAttribute-container">
                <div class="baseAttribute-item">
                    <label for="sku">SKU</label>
                    <input type="text" id="sku" name="sku" value="<?= $_SESSION['invalidData']['sku'][0] ?? NULL ?>">
                    <div class="ErrorMessage" id="SkuError"><?= $_SESSION['error']['sku'][0] ?? NULL ?></div>
                </div>
                <div class="baseAttribute-item">
                    <label for="name">Name</label>
                    <input type="text" id="name" name="name" value="<?= $_SESSION['invalidData']['name'][0] ?? NULL ?>">
                    <div class="ErrorMessage" id="NameError"><?= $_SESSION['error']['name'][0] ?? NULL ?></div>
                </div>
                <div class="baseAttribute-item">
                    <label for="price">Price</label>
                    <input type="text" id="price" name="price" value="<?= $_SESSION['invalidData']['price'][0] ?? NULL ?>">
                    <div class="ErrorMessage" id="PriceError"><?= $_SESSION['error']['price'][0] ?? NULL ?></div>

                </div>
                <div class="baseAttribute-item">
                    <label for="productType">Type Switcher</label>
                    <select id="productType" name="productType">
                        <option value="DVD">DVD-disc</option>
                        <option value="Book">Book</option>
                        <option value="Furniture">Furniture</option>
                    </select>
                    <?= $_SESSION['error']['productType'][0] ?? NULL ?>
                </div>
            </div>
            <div class="TypeSwitcher-Container">
                <div class="dvd-container" id="dvd-container">
                    <div class="specialAttribute-item">
                        <label for="size">Size</label>
                        <input type="text" id="size" name="size" value="<?= $_SESSION['invalidData']['size'][0] ?? NULL ?>">
                        <div class="ErrorMessage" id="SizeError"><?= $_SESSION['error']['size'][0] ?? NULL ?></div>

                    </div>
                    Size is in MB
                </div>
                <div class="book-container" id="book-container">
                    <div class="specialAttribute-item">
                        <label for="weight">Weight</label>
                        <input type="text" id="weight" name="weight" value="<?= $_SESSION['invalidData']['weight'][0] ?? NULL ?>">
                        <div class="ErrorMessage" id="WeightError"><?= $_SESSION['error']['weight'][0] ?? NULL ?></div>
                    </div>
                    Weight is in KG
                </div>
                <div class="furniture-container" id="furniture-container">
                    <div class="specialAttribute-item">
                        <label for="height">Height</label>
                        <input type="text" id="height" name="height" value="<?= $_SESSION['invalidData']['height'][0] ?? NULL ?>">
                        <div class="ErrorMessage" id="HeightError"><?= $_SESSION['error']['height'][0] ?? NULL ?></div>
                    </div>
                    <div class="specialAttribute-item">
                        <label for="width">Width</label>
                        <input type="text" id="width" name="width" value="<?= $_SESSION['invalidData']['width'][0] ?? NULL ?>">
                        <div class="ErrorMessage" id="WidthError"><?= $_SESSION['error']['width'][0] ?? NULL ?></div>
                    </div>
                    <div class="specialAttribute-item">
                        <label for="length">Length</label>
                        <input type="text" id="length" name="length" value="<?= $_SESSION['invalidData']['length'][0] ?? NULL ?>">
                        <div class="ErrorMessage" id="LengthError"><?= $_SESSION['error']['length'][0] ?? NULL ?></div>
                    </div>
                    Please provide dimensions in HxWxL format
                </div>
            </div>
        </main>
        <footer>
        </footer>
    </form>

    <script src="assets/type-switcher.js"></script>
    <script src="assets/form-validation.js"></script>

<?php require_once "layout/footer.php" ?>


