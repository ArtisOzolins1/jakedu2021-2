<?php require_once "layout/head.php" ?>

<?php
/**
 * @param $data array {
 *  productDTO: object {
 *      name: string,
 *      price: float,
 *      productType: string,
 *      size: int,
 *      weight: float,
 *      height: float,
 *      width: float,
 *      length: float
 *  }
 * }
 */
?>

<form id="product-list-form" action="/product-list/delete"  method="post">
    <header>
        <div class="header-container">
            <div class="title">Product List</div>
            <div class="button-prodList">
                <a href="/add-product">
                    <input type="button" value="ADD"></div>
                </a>
            <div class="button-prodList">
                <input id="delete-product-btn" type="submit" value="MASS DELETE">
            </div>
        </div>
    </header>
    <main>
        <div class="main-container-prodList">
            <?php if(empty($data)): ?>
                <div class="no-products-added">No products added</div>
            <?php endif; ?>
            <?php foreach ($data as $product): ?>
                <div class="product-container">
                    <div class="checkbox-container"><input type="checkbox" class="delete-checkbox" name="checkbox[]" value="<?= $product->id; ?>"></div>
                    <div class="product-item">SKU: <?= $product->sku; ?></div>
                    <div class="product-item">Name: <?= $product->name; ?></div>
                    <div class="product-item">Price: <?= $product->price; ?> $</div>
                    <?php if($product->productType === 'DVD') : ?>
                        <div class="product-item">Size: <?= $product->size; ?> MB</div>
                    <?php endif; ?>
                    <?php if($product->productType === 'Book') : ?>
                        <div class="product-item">Weight: <?= $product->weight; ?> KG</div>
                    <?php endif; ?>
                    <?php if($product->productType === 'Furniture') : ?>
                        <div class="product-item">Dimensions: <?= $product->height; ?>x<?= $product->width; ?>x<?= $product->length; ?></div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>

        </div>
    </main>
    <footer>
    </footer>
</form>
<script src="assets/product-list-validation.js"></script>

<?php require_once "layout/footer.php" ?>
