<?php

use Router\Handlers\ProductAdd;
use Router\Handlers\ProductList;
use Router\Handlers\ProductDelete;
use Router\Handlers\ProductSave;

/**
 * @var \Router\Router $router
 */

$router->get('/', ProductList::class);

$router->post('/product-list/delete', ProductDelete::class);

$router->get('/add-product', ProductAdd::class);

$router->post('/add-product/save', ProductSave::class);