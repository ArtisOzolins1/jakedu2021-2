const productForm = document.getElementById("product_form");

let validatedData = [];

class Validation {

    Valid(elementId, errorId, errorMsg)
    {
        let object = {
            elementId:elementId,
            errorId:errorId,
            errorMsg:errorMsg,
            valid: true
        };

        validatedData.push(object);
    }

    Invalid (elementId, errorId, errorMsg)
    {
        let object = {
            elementId:elementId,
            errorId:errorId,
            errorMsg:errorMsg,
            valid: false
        };

        validatedData.push(object);
    }

    isRequired(elementId, errorId, errorMsg)
    {
        const data = document.querySelector("#" + elementId).value;
        if (data.length<1) {
            return this.Invalid(elementId, errorId, errorMsg);
        }
        return this.Valid(elementId, errorId, errorMsg);
    }

    isRequiredLength(elementId, errorId, errorMsg, length)
    {
        const data = document.querySelector("#" + elementId).value;
        if (data.length<length) {
            return this.Invalid(elementId, errorId, errorMsg);
        }
        return this.Valid(elementId, errorId, errorMsg);
    }

    isNumeric(elementId, errorId, errorMsg)
    {
        const data = document.querySelector("#" + elementId).value;
        if (/^[1-9]\d*(\.\d+)?$/.test(data)) {
            return this.Valid(elementId, errorId, errorMsg);
        }
        return this.Invalid(elementId, errorId, errorMsg);
    }

    isAlphanumerical(elementId, errorId, errorMsg)
    {
        const data = document.querySelector("#" + elementId).value;
        if (/^[A-Za-z0-9 ]+$/.test(data)) {
            return this.Valid(elementId, errorId, errorMsg);
        }
        return this.Invalid(elementId, errorId, errorMsg);
    }

    renderError(elementId, errorId, errorMsg)
    {
        const dataElement = document.querySelector("#" + errorId)

        dataElement.innerHTML= errorMsg;
    }

    clearErrorAndArray()
    {
        Object.keys(validatedData).forEach(key => {
            const error = document.querySelector("#" + validatedData[key].errorId);
            error.innerHTML= "";
        });
        validatedData.length = 0;
    }

    validate(event) {
        Object.keys(validatedData).forEach(key => {
            if (validatedData[key].valid === false) {
                this.renderError(validatedData[key].elementId, validatedData[key].errorId, validatedData[key].errorMsg);
            }
        });
        let isInputValid = validatedData.every((i) => {
            return i.valid === true;
        });
        if (isInputValid === false) {
            event.preventDefault();
        }
    }

}

let validator = new Validation();

productForm.addEventListener('submit', function (event){

    validator.clearErrorAndArray();

    let typeswitcherValue = document.getElementById("productType").value;

    validator.isRequired('SKU', 'SkuError', 'Sku is required');
    validator.isAlphanumerical("SKU", 'SkuError', 'Sku is alphanumerical');
    validator.isRequired('Name', 'NameError', 'Name is required');
    validator.isAlphanumerical("Name", 'NameError', 'Name is alphanumerical');
    validator.isRequired('Price', 'PriceError', 'Price is required');
    validator.isNumeric('Price', 'PriceError', 'Price is numeric');
    if (typeswitcherValue === "DVD") {
        validator.isRequired('Size', 'SizeError', 'Size is required');
        validator.isNumeric('Size', 'SizeError', 'Size is numeric');
    }
    if (typeswitcherValue === "Book") {
        validator.isRequired('Weight', 'WeightError', 'Weight is required');
        validator.isNumeric('Weight', 'WeightError', 'Weight is numeric');
    }
    if (typeswitcherValue === "Furniture") {
        validator.isRequired('Height', 'HeightError', 'Height is required');
        validator.isNumeric('Height', 'HeightError', 'Height is numeric');
        validator.isRequired('Width', 'WidthError', 'Width is required');
        validator.isNumeric('Width', 'WidthError', 'Width is numeric');
        validator.isRequired('Length', 'LengthError', 'Length is required');
        validator.isNumeric('Length', 'LengthError', 'Length is numeric');
    }
    validator.validate(event);
});

