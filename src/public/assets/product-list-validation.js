document.getElementById("product-list-form").addEventListener("submit", function(e){
    let checkedCount = 0;
    let checkbox = document.getElementsByName('checkbox[]');
    let actionType = document.getElementById("delete-product-btn").value;
    for (let i = 0; i<checkbox.length;i++) {
        if(checkbox[i].checked){
            checkedCount ++;
        }
    }
    if (actionType === "MASS DELETE") {
        if (checkedCount < 1) {
            alert ("Must select one item at least to proceed");
            e.preventDefault();
        }
    }
});


