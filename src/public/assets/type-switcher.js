window.addEventListener('load', (event) => {
    document.getElementById("book-container").style.display = "none";
    document.getElementById("furniture-container").style.display = "none";
});
let selectElement = document.getElementById("productType");
let dvdElement = document.getElementById("dvd-container");
let bookElement = document.getElementById("book-container");
let furnitureElement = document.getElementById("furniture-container");

selectElement.addEventListener('change', (event) => {
    let typeswitcherValue = selectElement.value;

    if (typeswitcherValue === "DVD") {
        dvdElement.style.display = "block";
    } else {
        dvdElement.style.display = "none";
    }
    if (typeswitcherValue === "Book") {
        bookElement.style.display = "block";
    } else {
        bookElement.style.display = "none";
    }
    if (typeswitcherValue === "Furniture") {
        furnitureElement.style.display = "block";
    } else {
        furnitureElement.style.display = "none";
    }
});
